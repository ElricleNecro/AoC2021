local stack = {}

function stack.new(...)
	local new_stack = {...}

	setmetatable(new_stack, stack)

	return new_stack
end

function stack.is_empty(self)
	return #self == 0
end

function stack.push(self, value)
	table.insert(self, value)

	return value
end

function stack.pop(self)
	local value = self[#self]

	table.remove(self, #self)

	return value
end

function stack.peek(self)
        if self:is_empty() then
                return nil
        end

        return self[#self]
end

return stack
