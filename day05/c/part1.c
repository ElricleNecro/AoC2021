#ifndef INPUT_FILE
#define INPUT_FILE "day5/input"
#endif

#include "utils.c"

struct st_coord {
	int x, y;
};

struct st_edge {
	struct st_coord start, end;
};

struct st_array {
	size_t len;
	struct st_edge *array;
};

struct st_array array_allocate(size_t size) {
	return (struct st_array){.len = size, .array = calloc(size, sizeof(struct st_edge))};
}

void array_deallocate(struct st_array *data) {
	if( data == NULL ) {
		return;
	}

	free(data->array), data->len = 0;
}

struct st_array parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(stderr, "An error occured while opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct st_array data = array_allocate(line_count(file));

	for(size_t idx=0; idx < data.len; idx++)
		fscanf(file, "%d,%d -> %d,%d\n", &data.array[idx].start.x, &data.array[idx].start.y, &data.array[idx].end.x, &data.array[idx].end.y);

	return data;
}

int min_x(struct st_array data) {
	int min = data.array[0].start.x < data.array[0].end.x ? data.array[0].start.x : data.array[0].end.x;

	for(size_t idx=1; idx < data.len; idx++) {
		if( data.array[idx].start.x <= min )
			min = data.array[idx].start.x;

		if( data.array[idx].end.x <= min )
			min = data.array[idx].end.x;
	}

	return min;
}

int max_x(struct st_array data) {
	int max = data.array[0].start.x > data.array[0].end.x ? data.array[0].start.x : data.array[0].end.x;

	for(size_t idx=1; idx < data.len; idx++) {
		if( data.array[idx].start.x >= max )
			max = data.array[idx].start.x;

		if( data.array[idx].end.x >= max )
			max = data.array[idx].end.x;
	}

	return max;
}

int min_y(struct st_array data) {
	int min = data.array[0].start.y < data.array[0].end.y ? data.array[0].start.y : data.array[0].end.y;

	for(size_t idx=1; idx < data.len; idx++) {
		if( data.array[idx].start.y <= min )
			min = data.array[idx].start.y;

		if( data.array[idx].end.y <= min )
			min = data.array[idx].end.y;
	}

	return min;
}

int max_y(struct st_array data) {
	int max = data.array[0].start.y > data.array[0].end.y ? data.array[0].start.y : data.array[0].end.y;

	for(size_t idx=1; idx < data.len; idx++) {
		if( data.array[idx].start.y >= max )
			max = data.array[idx].start.y;

		if( data.array[idx].end.y >= max )
			max = data.array[idx].end.y;
	}

	return max;
}

int main(void) {
	struct st_array data = parse_file();
	struct st_coord start = {.x = min_x(data), .y = min_y(data)};
	struct st_coord end = {.x = max_x(data), .y = max_y(data)};
	size_t width = (end.y+1 - start.y), height = (end.x+1 - start.x);
	size_t *map = calloc(width * height, sizeof(size_t));

	for(size_t point=0; point < data.len; point++) {
		if( data.array[point].start.x == data.array[point].end.x ) {
			int step = (data.array[point].end.y - data.array[point].start.y) / abs(data.array[point].end.y - data.array[point].start.y);

			int start_y = data.array[point].start.y - start.y;
			int end_y = data.array[point].end.y - start.y;
			for(int y=start_y; y != end_y; y += step)
				map[y * width + data.array[point].start.x - start.x] += 1;
			if( start_y != end_y )
				map[end_y * width + data.array[point].start.x - start.x] += 1;
		} else if( data.array[point].start.y == data.array[point].end.y ){
			int step = (data.array[point].end.x - data.array[point].start.x) / abs(data.array[point].end.x - data.array[point].start.x);

			int start_x = data.array[point].start.x - start.x;
			int end_x = data.array[point].end.x - start.x;
			for(int x=start_x; x != end_x; x += step) {
				map[(data.array[point].start.y - start.y) * width + x] += 1;
			}
			if( start_x != end_x ) {
				map[(data.array[point].start.y - start.y) * width + end_x] += 1;
			}
		}
	}

	size_t count = 0;
	for(size_t idx=0; idx < width * height; idx++)
		if( map[idx] > 1 )
			count++;

	printf("Answer: %lu\n", count);

	array_deallocate(&data);

	return EXIT_SUCCESS;
}
