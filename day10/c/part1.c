#ifndef INPUT_FILE
#define INPUT_FILE "day10/input"
#endif

#include <stdio.h>

#include "utils.c"

STACK(char);

char **parse_file(size_t *lines) {
	FILE *file = fopen(INPUT_FILE, "r");
	*lines = line_count(file);

	char **data = calloc(*lines, sizeof(char*));

	for(size_t line_idx=0; line_idx<*lines; line_idx++) {
		size_t len = 0;
		if( getline(&data[line_idx], &len, file) <= 0 ) {
			printf("A problem occured.\n");
			for(size_t i=0; i<*lines; i++) {
				if( data[i] == NULL )
					break;
				free(data[i]);
			}
			free(data);
			fclose(file);
			exit(EXIT_FAILURE);
		}
		size_t str_len = strlen(data[line_idx]);
		if( data[line_idx][str_len-1] == '\n' )
			data[line_idx][str_len-1] = '\0';
	}

	fclose(file);
	return data;
}

void print_array(char *line, const size_t color) {
	printf("\r");
	for(size_t idx=0; idx < strlen(line); idx++) {
		if( color == idx )
			printf("\033[31m%c\033[00m", line[idx]);
		else
			printf("%c", line[idx]);
	}
}

int main(void) {
	size_t lines = 0;
	char **data = parse_file(&lines);
	size_t error_score = 0;

	for(size_t idx=0; idx<lines; idx++) {
		char *line = data[idx];
		Stack_char stack = new_stack_char();

		/*print_array(line, strlen(line));*/
		for(size_t pos=0; pos < strlen(line); pos++) {
			bool found_incorrect = false;
			char expected;
			switch(line[pos]) {
				case '{':
				case '(':
				case '[':
				case '<':
					push_stack_char(stack, line[pos]);
					break;
				case '}': {
						expected = pop_stack_char(stack);
						if( expected != '{' ) {
							found_incorrect = true;
							error_score += 1197;
						}
					}
					break;
				case ')': {
						expected = pop_stack_char(stack);
						if( expected != '(' ) {
							found_incorrect = true;
							error_score += 3;
						}
					}
					break;
				case ']': {
						expected = pop_stack_char(stack);
						if( expected != '[' ) {
							found_incorrect = true;
							error_score += 57;
						}
					}
					break;
				case '>': {
						expected = pop_stack_char(stack);
						if( expected != '<' ) {
							found_incorrect = true;
							error_score += 25137;
						}
					}
					break;

				default:
					printf("Something very weird happened!\n");
					exit(EXIT_FAILURE);
			}

			/*print_array(line, pos);*/
			if( found_incorrect ) {
				/*printf("\nFound '%c' instead of '%c'.", line[pos], expected);*/
				break;
			}
		}

		free_stack_char(stack);
		/*printf("\n");*/
	}

	printf("Answer: %lu\n", error_score);

	for(size_t idx=0; idx<lines; idx++)
		free(data[idx]);
	free(data);
	return EXIT_SUCCESS;
}
