#ifndef INPUT_FILE
#define INPUT_FILE "day10/input"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>

#include "utils.c"

STACK(char);

char **parse_file(size_t *lines) {
	FILE *file = fopen(INPUT_FILE, "r");
	*lines = line_count(file);

	char **data = calloc(*lines, sizeof(char*));

	for(size_t line_idx=0; line_idx<*lines; line_idx++) {
		size_t len = 0;
		if( getline(&data[line_idx], &len, file) <= 0 ) {
			printf("A problem occured.\n");
			for(size_t i=0; i<*lines; i++) {
				if( data[i] == NULL )
					break;
				free(data[i]);
			}
			free(data);
			fclose(file);
			exit(EXIT_FAILURE);
		}
		size_t str_len = strlen(data[line_idx]);
		if( data[line_idx][str_len-1] == '\n' )
			data[line_idx][str_len-1] = '\0';
	}

	fclose(file);
	return data;
}

void print_array(char *line, const size_t color) {
	printf("\r");
	for(size_t idx=0; idx < strlen(line); idx++) {
		if( color == idx )
			printf("\033[31m%c\033[00m", line[idx]);
		else
			printf("%c", line[idx]);
	}
}

int cmp(const void *a, const void *b) {
	size_t x = *(size_t*)a;
	size_t y = *(size_t*)b;

	if(x > y )
		return 1;
	else if( x < y )
		return -1;
	return 0;
}

int main(void) {
	size_t lines = 0;
	char **data = parse_file(&lines);
	size_t error_score = 0;
	size_t *total_scores = NULL;
	size_t len_total_score = 0;

	for(size_t idx=0; idx<lines; idx++) {
		char *line = data[idx];
		Stack_char stack = new_with_capacity_stack_char(strlen(line));
		bool found_incorrect = false;
		size_t total_score = 0;

		print_array(line, strlen(line));
		for(size_t pos=0; pos < strlen(line); pos++) {
			char expected;

			print_array(line, pos);

			switch(line[pos]) {
				case '{':
				case '(':
				case '[':
				case '<':
					push_stack_char(stack, line[pos]);
					break;
				case '}': {
						expected = pop_stack_char(stack);
						if( expected != '{' ) {
							found_incorrect = true;
							error_score += 1197;
						}
					}
					break;
				case ')': {
						expected = pop_stack_char(stack);
						if( expected != '(' ) {
							found_incorrect = true;
							error_score += 3;
						}
					}
					break;
				case ']': {
						expected = pop_stack_char(stack);
						if( expected != '[' ) {
							found_incorrect = true;
							error_score += 57;
						}
					}
					break;
				case '>': {
						expected = pop_stack_char(stack);
						if( expected != '<' ) {
							found_incorrect = true;
							error_score += 25137;
						}
					}
					break;

				default:
					printf("Something very weird happened!\n");
					exit(EXIT_FAILURE);
			}

			if( found_incorrect ) {
				printf("\nFound '%c' instead of '%c'.", line[pos], expected);
				break;
			}
		}
		printf("\n");

		if( found_incorrect )
			continue;

		while( stack->size > 0 ) {
			char elem = pop_stack_char(stack);
			size_t to_add = 0;
			switch(elem) {
				case '(':
					to_add = 1;
					break;
				case '[':
					to_add = 2;
					break;
				case '{':
					to_add = 3;
					break;
				case '<':
					to_add = 4;
					break;
				default:
					printf("Something very weird happened!\n");
					exit(EXIT_FAILURE);
			}

			total_score *= 5;
			total_score += to_add;
		}

		len_total_score += 1;
		total_scores = realloc(total_scores, sizeof(size_t) * len_total_score);
		total_scores[len_total_score-1] = total_score;

		free_stack_char(stack);
		free(stack);
	}

	printf("Error score: %lu\n", error_score);
	qsort(total_scores, len_total_score, sizeof(size_t), cmp);
	for(size_t idx=0; idx<len_total_score; idx++)
		printf("%lu ", total_scores[idx]);
	printf("\n");
	printf("Total score: %lu\n", total_scores[len_total_score / 2]);

	for(size_t idx=0; idx<lines; idx++)
		free(data[idx]);
	free(data);
	free(total_scores);
	return EXIT_SUCCESS;
}
