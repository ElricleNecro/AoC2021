#ifndef INPUT_FILE
#define INPUT_FILE "day8/input"
#endif

#include <assert.h>
#include <ctype.h>
#include <limits.h>

#include "stringview.h"
#include "utils.c"

#define BUF_SIZE 80 + 2 + 4*8 + 2
static const unsigned char SEGMENTS[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
static const size_t UNIQUE_LEN[] = {1, 4, 7, 8};
static const char *DISPLAY[] = {
	"abcefg",
	"cf",
	"acdeg",
	"acdfg",
	"bcdf",
	"abdfg",
	"abdefg",
	"acf",
	"abcdefg",
	"abcdfg"
};
static const char EXPECTED_WIRE_CONFIG[] = "abcdefg";
static const char CURRENT_WIRE_CONFIG[8] = {','};
static const unsigned char LOOK_UP[] = {2, 5, 6, 0, 1, 3, 4};

typedef struct pattern_st {
	StringView signal[10];
	StringView output[4];
} Pattern_t;

struct array parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(stderr, "Error while opening file '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	size_t to_read = line_count(file);
	fclose(file);

	struct array data = array_create(to_read * sizeof(Pattern_t));
	data.size = to_read;

	StringView file_content = {0};
	load_file(INPUT_FILE, &file_content);

	for(size_t line_idx=0; line_idx < to_read; line_idx++) {
		Pattern_t *line_data = &((Pattern_t*)data.data)[line_idx];

		StringView line = stringview_split(&file_content, '\n');
		bool output = false;

		for(size_t j=0; line.count > 0; j++) {
			StringView item = stringview_split_on_spaces(&line);

			if( *item.data == '|' ) {
				output = true;
				j = -1;
				continue;
			}

			if( ! output )
				line_data->signal[j] = item;
			else
				line_data->output[j] = item;
		}
	}

	return data;
}

int cmp(const void *a, const void *b) {
	char left = *(char*)a;
	char right = *(char*)b;

	return left - right;
}

size_t get_value_wire(const char *a, const size_t len) {
	for(size_t value=0; value < 10; value++)
		if( strncmp(a, DISPLAY[value], len) == 0 )
			return value;

	return LONG_MAX;
}

bool is_unique_len(StringView s) {
	for(size_t idx=0; idx < (sizeof(UNIQUE_LEN)/sizeof(size_t)); idx++)
		if( SEGMENTS[UNIQUE_LEN[idx]] == s.count )
			return true;
	return false;
}

size_t unique_len_pos(StringView s) {
	for(size_t idx=0; idx < (sizeof(UNIQUE_LEN)/sizeof(size_t)); idx++)
		if( SEGMENTS[UNIQUE_LEN[idx]] == s.count )
			return UNIQUE_LEN[idx];
	return sizeof(UNIQUE_LEN) / sizeof(size_t) + 1;
}

void work_on_unique(const size_t len, StringView data[len], char *configuration) {
	for(size_t idx=0; idx < len; idx++) {
		StringView elem = data[idx];

		if( is_unique_len(elem) ) {
			size_t correct_signal = unique_len_pos(elem);
			printf("\t'%.*s' -> '%s'\n", (int)elem.count, elem.data, DISPLAY[correct_signal]);
			for(size_t pos=0; pos < elem.count; pos++)
				if( configuration[elem.data[pos] - 'a'] == ',' ) {
					configuration[elem.data[pos] - 'a'] = DISPLAY[correct_signal][pos];
				}
		}
	}
}

bool match(const size_t len, const char src[len], const char expected[len]) {
	for(size_t pos=0; pos < len; pos++)
		if( src[pos] != ',' && src[pos] != expected[pos] )
			return false;
	return true;
}

void fill_in_blank(const size_t len, StringView data[len], char *configuration) {
	for(size_t idx=0; idx < len; idx++) {
		StringView elem = data[idx];

		if( is_unique_len(elem) )
			continue;

		char buf[8] = {0};
		for(size_t pos=0; pos < elem.count; pos++) {
			printf("%c -> %c\n", elem.data[pos], configuration[elem.data[pos] - 'a']);
			buf[pos] = configuration[elem.data[pos] - 'a'];
		}

		size_t count_possible = 0;
		for(size_t possiblity=0; possiblity < (sizeof(SEGMENTS) / sizeof(unsigned char)); possiblity++) {
			if( elem.count != SEGMENTS[possiblity] )
				continue;

			if( match(SEGMENTS[possiblity], buf, DISPLAY[possiblity]) )
				count_possible++;
		}

		if( count_possible == 0 ) {
			printf("'%s' ('%.*s') has no match.\n", buf, (int)elem.count, elem.data);
			exit(EXIT_FAILURE);
		}
	}
}

char* solve(struct array data) {
	char *configuration = malloc(8 * sizeof(char));
	for(size_t idx=0; idx < 8; idx++) {
		configuration[idx] = ',';
	}
	configuration[7] = '\0';

	printf("Before 1st pass, configuration is: '%s'\n", configuration);
	for(size_t line=0; line<data.size; line++) {
		Pattern_t line_data = ((Pattern_t*)data.data)[line];

		work_on_unique(10, line_data.signal, configuration);
		work_on_unique(4, line_data.output, configuration);
	}

	printf("After 1st pass, configuration is: '%s'\n", configuration);

	{
		size_t count_comma = 0;
		for(size_t pos=0; pos<7; pos++)
			if( configuration[pos] == ',' )
				count_comma++;

		if( count_comma == 0 )
			return configuration;
	}
	
	for(size_t line=0; line<data.size; line++) {
		Pattern_t line_data = ((Pattern_t*)data.data)[line];

		fill_in_blank(10, line_data.signal, configuration);
		fill_in_blank(4, line_data.output, configuration);
	}

	return configuration;
}

static const char config[] = "cfgabde";
int main(void) {
	struct array data = parse_file();

	size_t answer = 0;

	char *worked_out = solve(data);
	for(size_t pos=0; pos<8; pos++)
		printf("%c", worked_out[pos]);
	printf("\n");

	for(size_t line=0; line < data.size; line++) {
		Pattern_t line_data = ((Pattern_t*)data.data)[line];

		size_t value = 0;
		for(size_t output=0; output<4; output++) {
			const StringView elem = line_data.output[output];

			char segments[7] = {0};
			const size_t nb_elem = elem.count;
			for(size_t pos=0; pos < nb_elem; pos++) {
				segments[pos] = worked_out[elem.data[pos] - 'a'];
			}

			qsort(segments, nb_elem, sizeof(char), cmp);

			printf("-- %lu, %lu, (%.*s -> %.*s)\n", value, get_value_wire(segments, nb_elem), (int)nb_elem, elem.data, (int)nb_elem, segments);
			value = value * 10 + get_value_wire(segments, nb_elem);
		}

		printf("%lu\n", value);
		answer += value;
	}

	printf("Answer: %lu\n", answer);

	free(((Pattern_t*)data.data)[0].signal[0].data);
	free(worked_out);
	array_free(&data);

	return EXIT_SUCCESS;
}
