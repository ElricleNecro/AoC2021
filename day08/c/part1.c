#ifndef INPUT_FILE
#define INPUT_FILE "day8/input"
#endif

#include <ctype.h>

#include "stringview.h"
#include "utils.c"

#define BUF_SIZE 80 + 2 + 4*8 + 2
static const unsigned char SEGMENTS[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
static const size_t UNIQUE_LEN[] = {1, 4, 7, 8};
static const char *DISPLAY[] = {
	"abcefg",
	"cf",
	"acdeg",
	"acdfg",
	"bcdf",
	"abdfg",
	"abdefg",
	"acf",
	"abcdefg",
	"abcdfg"
};

typedef struct pattern_st {
	StringView signal[10];
	StringView output[4];
} Pattern_t;

struct array parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(stderr, "Error while opening file '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	size_t to_read = line_count(file);
	fclose(file);

	struct array data = array_create(to_read * sizeof(Pattern_t));
	data.size = to_read;

	StringView file_content = {0};
	load_file(INPUT_FILE, &file_content);

	for(size_t line_idx=0; line_idx < to_read; line_idx++) {
		Pattern_t *line_data = &((Pattern_t*)data.data)[line_idx];

		StringView line = stringview_split(&file_content, '\n');
		bool output = false;

		for(size_t j=0; line.count > 0; j++) {
			StringView item = stringview_split_on_spaces(&line);

			if( *item.data == '|' ) {
				output = true;
				j = -1;
				continue;
			}

			if( ! output )
				line_data->signal[j] = item;
			else
				line_data->output[j] = item;
		}
	}

	return data;
}

int main(void) {
	struct array data = parse_file();

	size_t unique_number = 0;

	for(size_t line=0; line < data.size; line++) {
		Pattern_t line_data = ((Pattern_t*)data.data)[line];

		for(size_t idx=0; idx < 4; idx++) {
			for(size_t uniq=0; uniq < sizeof(UNIQUE_LEN) / sizeof(size_t); uniq++) {
				if( line_data.output[idx].count == SEGMENTS[UNIQUE_LEN[uniq]] ) {
					unique_number++;
					break;
				}
			}
		}
	}

	printf("Answer: %lu\n", unique_number);

	return EXIT_SUCCESS;
}
