#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef INPUT_FILE
#define INPUT_FILE "day11/input"
#endif

#include <termios.h>
#include <unistd.h>
#include <time.h>

#include "utils.c"

ARRAY(int);
ARRAY(bool);

Array2d_int parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	size_t height = line_count(file);
	size_t width = line_width(file);

	Array2d_int data = {
		.width = width,
		.height = height,
		.size = width * height,
		.data = calloc(width * height, sizeof(int)),
	};

	char *buf = NULL;
	size_t line_len = 0;
	for(size_t line=0; line < height; line++) {
		ssize_t read = getline(&buf, &line_len, file);
		if( read <= 0 ) {
			printf("A problem occured.");
			free(data.data);
			fclose(file);
			if( buf != NULL )
				free(buf);
			exit(EXIT_FAILURE);
		}

		if( buf[strlen(buf) - 1] == '\n' )
			buf[strlen(buf) - 1] = '\0';

		for(size_t idx=0; idx < width; idx++)
			data.data[line * width + idx] = buf[idx] - '0';
	}

	free(buf);
	fclose(file);

	return data;
}

void print_array(Array2d_int data, const int cursor_x, const int cursor_y) {
	for(size_t y=0; y < data.height; y++) {
		gotoxy(cursor_x, (int)(cursor_y + y));
		for(size_t x=0; x < data.width; x++) {
			printf("%d", data.data[y * data.width + x]);
		}
		printf("\n");
	}
}

void colored_print_array(Array2d_int data, Array2d_bool flashed, const int cursor_x, const int cursor_y) {
	for(size_t y=0; y < data.height; y++) {
		gotoxy(cursor_x, (int)(cursor_y + y));
		for(size_t x=0; x < data.width; x++) {
			if( flashed.data[y * flashed.width + x] )
				printf("\033[32m%d\033[00m", data.data[y * data.width + x]);
			else
				printf("%d", data.data[y * data.width + x]);
		}
	}
}

void propagate_flash(Array2d_int data, const size_t x, const size_t y) {
	if( x > 0 )
		data.data[y * data.width + x - 1]++;
	if( x < data.width - 1 )
		data.data[y * data.width + x + 1]++;

	if( x > 0 && y > 0 )
		data.data[(y - 1) * data.width + x - 1]++;
	if( x > 0 && y < data.height - 1 )
		data.data[(y + 1) * data.width + x - 1]++;

	if( x < data.width - 1 && y > 0 )
		data.data[(y - 1) * data.width + x + 1]++;
	if( x < data.width - 1 && y < data.height - 1 )
		data.data[(y + 1) * data.width + x + 1]++;

	if( y > 0 )
		data.data[(y - 1) * data.width + x]++;
	if( y < data.width - 1 )
		data.data[(y + 1) * data.width + x]++;
}

Array2d_bool create_from(Array2d_int from) {
	Array2d_bool data = {
		.width = from.width,
		.height = from.height,
		.size = from.size,
		.data = calloc(from.size, sizeof(bool)),
	};

	return data;
}

bool check_propagation(Array2d_int data, Array2d_bool flashed) {
	for(size_t idx=0; idx < data.size; idx++)
		if( data.data[idx] > 9 && !flashed.data[idx] )
			return true;

	return false;
}

size_t evolve(Array2d_int data, Array2d_bool flashed) {
	for(size_t idx=0; idx < data.size; idx++) {
		data.data[idx] += 1;
		flashed.data[idx] = false;
	}

	do {
		for(size_t y=0; y < data.height; y++) {
			for(size_t x=0; x < data.width; x++) {
				if( !flashed.data[y * data.width + x] && data.data[y * data.width + x] > 9 ) {
					propagate_flash(data, x, y);
					flashed.data[y * data.width + x] = true;
				}
			}
		}
	} while( check_propagation(data, flashed) );

	for(size_t idx=0; idx < data.size; idx++)
		if( data.data[idx] > 9 )
			data.data[idx] = 0;

	size_t number_flash = 0;
	for(size_t idx=0; idx < flashed.size; idx++)
		if( flashed.data[idx] )
			number_flash++;

	return number_flash;
}

int main(void) {
	Array2d_int data = parse_file();
	Array2d_bool flashed = create_from(data);
	size_t nb_flash = 0;

	struct termios orig, term;
	tcgetattr(STDIN_FILENO, &term);
	orig = term;

	term.c_lflag &= !(ECHO | ICANON | IEXTEN | ISIG);
	term.c_iflag &= !(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	term.c_oflag &= !(OPOST);
	term.c_cflag |= CS8;

	term.c_cc[VMIN] = 0;
	term.c_cc[VTIME] = 1;

	tcsetattr(STDIN_FILENO, TCSAFLUSH, &term);

	printf("\x1b[2J");
	printf("\x1b[H");

	int x, y;
	getxy(&x, &y);

	gotoxy(x, y); printf("Step %lu:\n", 0L); print_array(data, x, y+1);
	for(size_t step=1; step < 101; step++) {
		gotoxy(x, y); printf("Step %lu:\n", step);
		nb_flash += evolve(data, flashed);
		colored_print_array(data, flashed, x, y + 1);

		struct timespec time = {.tv_sec = 0, .tv_nsec = 10000000};
		nanosleep(&time, NULL);
	}

	int new_x, new_y;
	getxy(&new_x, &new_y);
	gotoxy(x, new_y + 1);

	orig.c_iflag |= BRKINT | ICRNL | INPCK | ISTRIP | IXON;
	orig.c_oflag |= OPOST;      // Terminal is not restored if used...
	orig.c_lflag |= ECHO | ICANON | IEXTEN | ISIG;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig);
	
	printf("Answer: %lu\n", nb_flash);

	return EXIT_SUCCESS;
}
