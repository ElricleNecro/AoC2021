#include "utils.c"

struct bits {
	size_t zero;
	size_t one;
};

ChainedLst parse_file(void) {
	FILE *file = NULL;

	if( (file = fopen(INPUT_FILE, "r")) == NULL ) {
		fprintf(stderr, "Error while opening file: '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	ChainedLst data = lst_new();
	size_t size = line_count(file);
	for(size_t idx=0; idx < size; idx++) {
		char *val = calloc(64, sizeof(char));
		if( val == NULL ) {
			fprintf(stderr, "Error while allocating memory: %s\n", strerror(errno));
			fclose(file);
			lst_free(data, free);
			exit(EXIT_FAILURE);
		}

		fscanf(file, "%s\n", val);

		data = lst_append(data, val);
	}

	fclose(file);
	return data;
}

char* apply_bit_criteria(const ChainedLst data, const size_t bit, bool (*select_ones)(struct bits), bool free_data) {
	if( data->len == 1 ) {
		char *rating = data->start->data;
		if( free_data )
			lst_free(data, NULL);
		return rating;
	}

	struct bits stats = {0};
	ChainedLst ones = lst_new(), zeros = lst_new();

	size_t position = 0;
	for(ChainedLst_elem lst_elem = data->start; lst_elem != NULL; lst_elem = lst_elem->next,position++) {
		char *report = lst_elem->data;

		if( report[bit] == '0' ) {
			stats.zero++;
			zeros = lst_append(zeros, report);
		}
		else if( report[bit] == '1' ) {
			stats.one++;
			ones = lst_append(ones, report);
		}
	}

	printf("%lu, %lu => %s\n", stats.one, stats.zero, select_ones(stats) ? "ones" : "zeros");
	if( select_ones(stats) ) {
		if( free_data )
			lst_free(data, NULL);
		lst_free(zeros, NULL);
		return apply_bit_criteria(ones, bit + 1, select_ones, true);
	} else {
		if( free_data )
			lst_free(data, NULL);
		lst_free(ones, NULL);
		return apply_bit_criteria(zeros, bit + 1, select_ones, true);
	}
}

bool select_ones_for_oxygen(struct bits stat) {
	return stat.one >= stat.zero;
}

bool select_ones_for_co2(struct bits stat) {
	return stat.one < stat.zero;
}

int main(void) {
	ChainedLst data = parse_file();

	printf("1, 0\n");
	char *oxygen_rating = apply_bit_criteria(data, 0, select_ones_for_oxygen, false);
	printf("oxygen: %s\n", oxygen_rating);
	printf("1, 0\n");
	char *co2_rating = apply_bit_criteria(data, 0, select_ones_for_co2, false);
	printf("co2: %s\n", co2_rating);

	size_t col_len = strlen(oxygen_rating);
	size_t oxygen = 0;
	for(size_t idx=0; idx < col_len; idx++)
		if( oxygen_rating[idx] == '1' )
			oxygen |= 1 << (col_len - 1 - idx);

	col_len = strlen(co2_rating);
	size_t co2 = 0;
	for(size_t idx=0; idx < col_len; idx++)
		if( co2_rating[idx] == '1' )
			co2 |= 1 << (col_len - 1 - idx);

	printf("oxygen * co2 = %lu * %lu = %lu\n", oxygen, co2, oxygen * co2);

	lst_free(data, free);
	return EXIT_SUCCESS;
}
