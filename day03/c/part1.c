#include "utils.c"

struct bits {
	size_t zero;
	size_t one;
};

ChainedLst parse_file(void) {
	FILE *file = NULL;

	if( (file = fopen(INPUT_FILE, "r")) == NULL ) {
		fprintf(stderr, "Error while opening file: '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	ChainedLst data = lst_new();
	size_t size = line_count(file);
	for(size_t idx=0; idx < size; idx++) {
		char *val = calloc(64, sizeof(char));
		if( val == NULL ) {
			fprintf(stderr, "Error while allocating memory: %s\n", strerror(errno));
			fclose(file);
			lst_free(data, free);
			exit(EXIT_FAILURE);
		}

		fscanf(file, "%s\n", val);

		data = lst_append(data, val);
	}

	fclose(file);
	return data;
}

int main(void) {
	ChainedLst data = parse_file();

	size_t col_len = strlen(data->start->data);
	struct bits *stats = calloc(col_len, sizeof(struct bits));

	for(ChainedLst_elem lst_elem = data->start; lst_elem != NULL; lst_elem = lst_elem->next) {
		char *report = lst_elem->data;
		for(size_t idx=0; idx < col_len; idx++) {
			if( report[idx] == '0' )
				stats[idx].zero++;
			else if( report[idx] == '1' )
				stats[idx].one++;
		}
	}

	size_t gamma = 0, epsilon = 0;
	for(size_t idx=0; idx < col_len; idx++) {
		if( stats[idx].one >= stats[idx].zero )
			gamma |= 1 << (col_len - idx - 1);
		else
			epsilon |= 1 << (col_len - idx - 1);
	}

	printf("Answer: %lu\n", gamma * epsilon);
	
	free(stats);
	lst_free(data, free);
	return EXIT_SUCCESS;
}
