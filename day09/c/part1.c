#ifndef INPUT_FILE
#define INPUT_FILE "day9/input"
#endif

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>

#include "utils.c"

typedef struct array2d_t {
	size_t *data;
	size_t width, height, size;
} Array2d;

Array2d parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	size_t height = line_count(file);
	size_t width = line_width(file);

	printf("Found: %lux%lu\n", width, height);

	Array2d data = {
		.data = calloc(height * width, sizeof(size_t)),
		.width = width,
		.height = height,
		.size = width * height
	};

	char *buf = calloc(width, sizeof(char));
	size_t tmp_len = width;
	for(size_t line=0; line < height; line++) {
		size_t len = getline(&buf, &tmp_len, file);
		if( len <= 0 ) {
			printf("A problem occured: %lu\n", len);
			fclose(file);
			free(buf);
			free(data.data);
			exit(EXIT_FAILURE);
		}

		if( buf[len-1] == '\n' )
			len--;

		if( len != width ) {
			printf("A problem occured: %lu != %lu\n", tmp_len, width);
			fclose(file);
			free(buf);
			free(data.data);
			exit(EXIT_FAILURE);
		}

		for(size_t i=0; i < width; i++) {
			data.data[line * width + i] = buf[i] - '0';
		}
	}

	fclose(file);
	free(buf);
	return data;
}

bool is_lowest(Array2d data, size_t x, size_t y) {
	size_t value = data.data[y * data.width + x];
	bool lower = true;

	if( x > 0 )
		lower &= data.data[y * data.width + x - 1] > value;
	if( x < data.width - 1 )
		lower &= data.data[y * data.width + x + 1] > value;

	if( y > 0 )
		lower &= data.data[(y - 1) * data.width + x] > value;
	if( y < data.height - 1 )
		lower &= data.data[(y + 1) * data.width + x] > value;

	return lower;
}

int main(void) {
	Array2d data = parse_file();

	printf("Matrix: %lux%lu = %lu\n", data.width, data.height, data.size);

	size_t sum_risk = 0;
	for(size_t i = 0; i < data.height; i++) {
		for(size_t j=0; j < data.width; j++) {
			if( is_lowest(data, j, i) ) {
				sum_risk += data.data[data.width * i + j] + 1;
				/*printf("\033[31m%lu\033[00m ", data.data[data.width * i + j]);*/
			}/* else {*/
				/*printf("%lu ", data.data[data.width * i + j]);*/
			/*}*/
		}
		/*printf("\n");*/
	}

	printf("Answer: %lu\n", sum_risk);

	free(data.data);
	return EXIT_SUCCESS;
}
