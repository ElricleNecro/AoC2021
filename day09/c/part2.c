#ifndef INPUT_FILE
#define INPUT_FILE "day9/input"
#endif

#include "utils.c"

ARRAY(size_t);
ARRAY(bool);

Array2d_size_t parse_file(void) {
	FILE *file = fopen(INPUT_FILE, "r");
	size_t height = line_count(file);
	size_t width = line_width(file);

	printf("Found: %lux%lu\n", width, height);

	Array2d_size_t data = {
		.data = calloc(height * width, sizeof(size_t)),
		.width = width,
		.height = height,
		.size = width * height
	};

	char *buf = calloc(width, sizeof(char));
	size_t tmp_len = width;
	for(size_t line=0; line < height; line++) {
		size_t len = getline(&buf, &tmp_len, file);
		if( len <= 0 ) {
			printf("A problem occured: %lu\n", len);
			fclose(file);
			free(buf);
			free(data.data);
			exit(EXIT_FAILURE);
		}

		if( buf[len-1] == '\n' )
			len--;

		if( len != width ) {
			printf("A problem occured: %lu != %lu\n", tmp_len, width);
			fclose(file);
			free(buf);
			free(data.data);
			exit(EXIT_FAILURE);
		}

		for(size_t i=0; i < width; i++) {
			data.data[line * width + i] = buf[i] - '0';
		}
	}

	fclose(file);
	free(buf);
	return data;
}

typedef struct queue_node_t {
	size_t x, y;
	struct queue_node_t *next, *prev;
} Node;

typedef struct queue_t {
	struct queue_node_t *top, *bottom;
	size_t size;
} *Queue;

Queue queue_new(void) {
	Queue qnew = malloc(sizeof(struct queue_t));

	qnew->top = NULL;
	qnew->bottom = NULL;
	qnew->size = 0;

	return qnew;
}

Queue queue_append(Queue queue, size_t x, size_t y) {
	Node *node = malloc(sizeof(struct queue_node_t));
	node->x = x;
	node->y = y;
	node->next = NULL;
	node->prev = queue->bottom;

	queue->size += 1;
	if( queue->bottom != NULL )
		queue->bottom->next = node;
	queue->bottom = node;
	if( queue->top == NULL )
		queue->top = node;

	return queue;
}

Queue queue_pop(Queue queue, size_t *x, size_t *y) {
	if( queue->top == NULL )
		return queue;

	Node *node = queue->top;
	queue->top = node->next;
	if( queue->bottom == node )
		queue->bottom = NULL;
	queue->size--;

	*x = node->x;
	*y = node->y;

	free(node);

	return queue;
}

Queue queue_free(Queue queue) {
	if( queue->size == 0 || queue->top == NULL || queue->bottom == NULL )
		return queue;

	Node *node = queue->top;
	while(node != NULL) {
		Node *tmp = node;
		node = node->next;
		node->prev = NULL;
		queue->size--;

		free(tmp);
	}

	return queue;
}

bool is_lowest(Array2d_size_t data, size_t x, size_t y) {
	size_t value = data.data[y * data.width + x];
	bool lower = true;

	if( x > 0 )
		lower &= data.data[y * data.width + x - 1] > value;
	if( x < data.width - 1 )
		lower &= data.data[y * data.width + x + 1] > value;

	if( y > 0 )
		lower &= data.data[(y - 1) * data.width + x] > value;
	if( y < data.height - 1 )
		lower &= data.data[(y + 1) * data.width + x] > value;

	return lower;
}

bool is_unvisited(Array2d_bool visited) {
	bool is_there = false;

	for(size_t idx = 0; idx < visited.height; idx++) {
		is_there |= visited.data[idx];
	}

	return is_there;
}

size_t process_island(Array2d_bool visited, size_t start_x, size_t start_y) {
	size_t size = 0;

	Queue queue = queue_new();

	queue = queue_append(queue, start_x, start_y);

	while( queue->size != 0 ) {
		size_t x, y;
		queue = queue_pop(queue, &x, &y);
		if( visited.data[y * visited.width + x] )
			continue;

		visited.data[y * visited.width + x] = true;
		size++;

		if( x > 0 && ! visited.data[y * visited.width + x - 1] )
			queue = queue_append(queue, x-1, y);
		if( x < visited.width - 1 && ! visited.data[y * visited.width + x + 1])
			queue = queue_append(queue, x+1, y);

		if( y > 0 && ! visited.data[(y - 1) * visited.width + x] )
			queue = queue_append(queue, x, y-1);
		if( y < visited.height - 1 && ! visited.data[(y + 1) * visited.width + x] )
			queue = queue_append(queue, x, y+1);
	}

	queue = queue_free(queue);
	free(queue);

	return size;
}

int cmp(const void *a, const void *b) {
	size_t x = *(size_t*)a;
	size_t y = *(size_t*)b;

	return -(x - y);
}

int main(void) {
	Array2d_size_t data = parse_file();

	Array2d_bool visited = {
		.width = data.width,
		.height = data.height,
		.size = data.size,
		.data = calloc(data.size, sizeof(bool)),
	};

	for(size_t idx = 0; idx < data.size; idx++) {
		visited.data[idx] = data.data[idx] == 9 ? true : false;
	}

	/*for(size_t y=0; y<visited.height; y++) {*/
		/*for(size_t x=0; x<visited.width; x++) {*/
			/*if( visited.data[y * visited.width + x] )*/
				/*printf("\033[32mT\033[00m");*/
			/*else*/
				/*printf("\033[31mF\033[00m");*/
		/*}*/
		/*printf("\n");*/
	/*}*/

	size_t *island_size = NULL, island = 0;
	for(size_t starter=0; starter < visited.size && is_unvisited(visited); starter++) {
		if( visited.data[starter] )
			continue;

		island++;
		island_size = realloc(island_size, island * sizeof(size_t));
		island_size[island-1] = process_island(visited, starter % data.width, starter / data.width);
	}

	qsort(island_size, island, sizeof(size_t), cmp);
	size_t answer = island_size[0] * island_size[1] * island_size[2];
	printf("Answer: %lu\n", answer);

	free(island_size);
	free(data.data);
	free(visited.data);
	return EXIT_SUCCESS;
}
