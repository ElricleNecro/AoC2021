#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#ifndef INPUT_FILE
#define INPUT_FILE "day12/input"
#endif

#ifndef BUF_CHAR_SIZE
#define BUF_CHAR_SIZE 6
#endif

#include "utils.c"

ARRAY(char);
ARRAY1D(bool);
LIST(char);

typedef struct node_t {
	char *name;
	size_t neighbors_number;
	char **neighbors;
} *Node;

void node_add_neighbor(Node node, char *name) {
	for(size_t idx=0; idx < node->neighbors_number; idx++)
		if( strcmp(node->neighbors[idx], name) == 0 )
			return ;

	node->neighbors_number++;
	node->neighbors = realloc(node->neighbors, node->neighbors_number * sizeof(char*));

	node->neighbors[node->neighbors_number - 1] = name;
}

void node_free(struct node_t node) {
	free(node.name);
	free(node.neighbors);
}

typedef struct graph_t {
	struct node_t *node_lst;
	size_t node_number;

	Array2d_char adjency;
} *Graph;

Graph graph_new(void) {
	Graph gnew = malloc(sizeof(struct graph_t));

	gnew->node_lst = NULL;
	gnew->node_number = 0;

	gnew->adjency.data = NULL;
	gnew->adjency.width = 0;
	gnew->adjency.height = 0;
	gnew->adjency.size = 0;

	return gnew;
}

Node graph_add_node(Graph graph, char *name) {
	for(size_t idx=0; idx < graph->node_number; idx++) {
		if( strcmp(graph->node_lst[idx].name, name) == 0 )
			return &graph->node_lst[idx];
	}

	graph->node_number++;
	graph->node_lst = realloc(graph->node_lst, graph->node_number * sizeof(struct node_t));

	graph->node_lst[graph->node_number - 1].name = name;

	return &graph->node_lst[graph->node_number - 1];
}

Node graph_get_node(Graph graph, const char *name) {
	for(size_t idx=0; idx < graph->node_number; idx++) {
		if( strcmp(graph->node_lst[idx].name, name) == 0 )
			return &graph->node_lst[idx];
	}

	return NULL;
}

size_t graph_get_node_index(Graph graph, char *name) {
	for(size_t idx=0; idx < graph->node_number; idx++) {
		if( strcmp(graph->node_lst[idx].name, name) == 0 )
			return idx;
	}

	printf("Something went terribly wrong: '%s' is not in the graph.\n", name);
	exit(EXIT_FAILURE);
}

void graph_build_adjency(Graph graph) {
	graph->adjency.width = graph->node_number;
	graph->adjency.height = graph->node_number;
	graph->adjency.size = graph->adjency.width * graph->adjency.height;

	graph->adjency.data = calloc(graph->adjency.size, sizeof(char));

	for(size_t node_idx=0; node_idx < graph->node_number; node_idx++) {
		const struct node_t current = graph->node_lst[node_idx];
		for(size_t ndx=0; ndx < current.neighbors_number; ndx++) {
			size_t neighbor_idx = graph_get_node_index(graph, current.neighbors[ndx]);
			graph->adjency.data[node_idx * graph->node_number + neighbor_idx] = 1;
		}
	}
}

void graph_print_adjency_list(Graph graph) {
	for(size_t idx=0; idx < graph->node_number; idx++) {
		printf("%s: [", graph->node_lst[idx].name);
		for(size_t ndx=0; ndx < graph->node_lst[idx].neighbors_number; ndx++) {
			printf("%s", graph->node_lst[idx].neighbors[ndx]);
			if( ndx < graph->node_lst[idx].neighbors_number - 1 )
				printf(", ");
		}
		printf("]\n");
	}
}

void graph_print_adjency_matrix(Graph graph) {
	printf("%*s ", BUF_CHAR_SIZE, " ");
	for(size_t ndx=0; ndx < graph->node_number; ndx++)
		printf("%*s ", BUF_CHAR_SIZE, graph->node_lst[ndx].name);
	printf("\n");
	for(size_t y=0; y < graph->adjency.height; y++) {
		printf("%*s ", BUF_CHAR_SIZE, graph->node_lst[y].name);
		for(size_t x=0; x < graph->adjency.width; x++) {
			printf("% *d ", BUF_CHAR_SIZE, graph->adjency.data[y * graph->adjency.width + x]);
		}
		printf("\n");
	}
}

void print_path(char **path, const size_t len) {
	for(size_t idx=0; idx < len; idx++) {
		printf("%s", path[idx]);
		if( idx < len - 1 )
			printf(" -> ");
	}
	printf("\n");
}

bool is_in_path(char **path, const size_t path_length, const char *name) {
	for(size_t idx=0; idx < path_length; idx++)
		if( strcmp(path[idx], name) == 0 )
			return true;
	return false;
}

size_t is_in_path_many_times(char **path, const size_t path_length, const char *name) {
	size_t count = 0;
	for(size_t idx=0; idx < path_length; idx++)
		if( strcmp(path[idx], name) == 0 )
			count++;

	return count;
}

bool is_lower_name(const char *name) {
	if( 'a' <= name[0] && name[0] <= 'z' )
		return true;

	return false;
}

bool is_valid(char **path, const size_t path_length, const char *name) {
	// Special cases: start and end must only be present once:
	// start is already present because of the init:
	if( strcmp(name, "start") == 0 )
		return false;

	if( strcmp(name, "end") == 0 && is_in_path(path, path_length, "end") )
		return false;

	// If this is a small cave:
	if( is_lower_name(name) ) {
		// that is not already in the path:
		if( ! is_in_path(path, path_length, name) )
			return true;

		// that is not already present twice or more:
		if( is_in_path_many_times(path, path_length, name) >= 2 ) {
			return false;
		}

		// No other small caves was visited twice:
		for(size_t pdx=0; pdx < path_length; pdx++) {
			// We only apply a limit on small caves:
			if( ! is_lower_name(path[pdx]) )
				continue;

			// One is already present twice or more, then name is not to be re-added:
			if( is_in_path_many_times(path, path_length, path[pdx]) >= 2 )
				return false;
		}
	}

	return true;
}

List2d_char graph_go_through(Graph graph, List2d_char paths, const size_t from, const char *node_name, const char *end) {
	size_t orig_size = paths.sizes[from];
	char **orig_from = calloc(paths.sizes[from], sizeof(char*));
	memcpy(orig_from, paths.data[from], paths.sizes[from] * sizeof(char*));

	Node start_node = graph_get_node(graph, node_name);

	if( is_valid(paths.data[from], paths.sizes[from], start_node->neighbors[0]) ) {
		paths.sizes[from]++;
		paths.data[from] = realloc(paths.data[from], paths.sizes[from] * sizeof(char*));
		paths.data[from][paths.sizes[from] - 1] = start_node->neighbors[0];

		if( strcmp(start_node->neighbors[0], end) != 0 )
			paths = graph_go_through(graph, paths, from, start_node->neighbors[0], end);
	}

	for(size_t ndx=1; ndx < start_node->neighbors_number; ndx++) {
		if( ! is_valid(orig_from, orig_size, start_node->neighbors[ndx]) )
			continue;

		paths.length += 1;
		paths.data = realloc(paths.data, paths.length * sizeof(char**));
		paths.sizes = realloc(paths.sizes, paths.length * sizeof(char**));

		paths.sizes[paths.length - 1] = orig_size + 1;
		paths.data[paths.length - 1] = calloc(paths.sizes[paths.length - 1], sizeof(char*));
		memcpy(paths.data[paths.length - 1], orig_from, orig_size * sizeof(char*));
		paths.data[paths.length - 1][orig_size] = start_node->neighbors[ndx];

		if( strcmp(start_node->neighbors[ndx], end) != 0 )
			paths = graph_go_through(graph, paths, paths.length - 1, start_node->neighbors[ndx], end);
	}

	return paths;
}

List2d_char graph_paths(Graph graph, const char *start, const char *end) {
	List2d_char paths = {
		.data = NULL,
		.sizes = NULL,
		.length = 0,
	};
	Node start_node = graph_get_node(graph, start);

	for(size_t ndx=0; ndx < start_node->neighbors_number; ndx++) {
		paths.length += 1;
		paths.data = realloc(paths.data, paths.length * sizeof(char**));
		paths.sizes = realloc(paths.sizes, paths.length * sizeof(char**));

		paths.sizes[paths.length - 1] = 2;
		paths.data[paths.length - 1] = calloc(paths.sizes[paths.length - 1], sizeof(char*));
		paths.data[paths.length - 1][0] = start;
		paths.data[paths.length - 1][1] = start_node->neighbors[ndx];

		if( strcmp(start_node->neighbors[ndx], end) != 0 )
			paths = graph_go_through(graph, paths, paths.length - 1, start_node->neighbors[ndx], end);
	}

	return paths;
}

void graph_free(Graph graph) {
	for(size_t ndx=0; ndx < graph->node_number; ndx++) {
		node_free(graph->node_lst[ndx]);
	}

	free(graph->node_lst);
	free(graph->adjency.data);
}

void split_on(const char *s, char split, char *restrict left, char *restrict right) {
	size_t sdx=0;
	for(sdx=0; sdx < strlen(s); sdx++) {
		if( s[sdx] == split )
			break;
	}

	memcpy(left, s, sdx * sizeof(char));
	memcpy(right, &s[sdx+1], (strlen(s) - (sdx+1)) * sizeof(char));
}

Graph parse_file(void) {
	Graph data = graph_new();

	FILE *file = fopen(INPUT_FILE, "r");
	size_t lines = line_count(file);

	char *line_data = calloc(BUF_CHAR_SIZE + 2, sizeof(char));
	for(size_t line=0; line < lines; line++) {
		char *lnode = calloc(BUF_CHAR_SIZE, sizeof(char)),
		     *rnode = calloc(BUF_CHAR_SIZE, sizeof(char));

		fscanf(file, "%s\n", line_data);

		split_on(line_data, '-', lnode, rnode);

		Node left = graph_add_node(data, lnode);
		node_add_neighbor(left, rnode);
		Node right = graph_add_node(data, rnode);
		node_add_neighbor(right, lnode);
	}

	graph_build_adjency(data);

	fclose(file);
	return data;
}

int main(void) {
	Graph graph = parse_file();

	graph_print_adjency_list(graph);
	graph_print_adjency_matrix(graph);

	List2d_char paths = graph_paths(graph, "start", "end");

	size_t valid_path = 0;
	for(size_t idx=0; idx < paths.length; idx++) {
		if( strcmp(paths.data[idx][paths.sizes[idx] - 1], "end") == 0 ) {
			print_path(paths.data[idx], paths.sizes[idx]);
			valid_path++;
		}
	}

	printf("Answer: %lu\n", valid_path);

	graph_free(graph);
	free(graph);

	for(size_t pdx=0; pdx < paths.length; pdx++) {
		free(paths.data[pdx]);
	}
	free(paths.sizes);
	free(paths.data);

	return EXIT_SUCCESS;
}
