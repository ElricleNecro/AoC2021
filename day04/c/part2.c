#ifndef INPUT_FILE
#define INPUT_FILE "day4/input"
#endif

#define BOARD_SIZE 5

#include "utils.c"

typedef struct _chained_lst_t_int {
	size_t data;

	struct _chained_lst_t_int *next, *prev;
} *ChainedLst_int_elem;

typedef struct _chaine_lst_root_t_int {
	size_t len;

	struct _chained_lst_t_int *start, *end;
} *ChainedLstInt;

ChainedLstInt lst_int_new(void) {
	ChainedLstInt root = malloc(sizeof(struct _chaine_lst_root_t_int));

	root->len = 0;
	root->start = NULL;
	root->end = NULL;

	return root;
}

ChainedLstInt lst_int_append(ChainedLstInt root, size_t data) {
	if( root == NULL ) {
		root = lst_int_new();
	}

	ChainedLst_int_elem new = malloc(sizeof(struct _chained_lst_t_int));

	new->data = data;

	if( root->end != NULL )
		root->end->next = new;

	if( root->start == NULL )
		root->start = new;

	new->prev = root->end;
	new->next = NULL;

	root->end = new;
	root->len++;

	return root;
}

bool lst_int_in(ChainedLstInt root, size_t data) {
	if( root == NULL )
		return false;

	for(ChainedLst_int_elem elem = root->start; elem != NULL; elem = elem->next) {
		if( data == elem->data )
			return true;
	}

	return false;
}

void lst_int_free(ChainedLstInt root) {
	if( root == NULL )
		return ;

	ChainedLst_int_elem elem = root->start;
	while(elem != NULL) {
		ChainedLst_int_elem cur = elem;
		elem = elem->next;
		free(cur);
	}

	free(root);
}


typedef struct st_game {
	size_t draw_length;
	size_t *draw;

	size_t boards_number;
	size_t *boards;
} Game;

typedef struct st_status {
	size_t board_id;
	size_t last;
	bool marked[BOARD_SIZE * BOARD_SIZE];
} Status;

size_t file_size(FILE *stream) {
	size_t pos = ftell(stream);
	fseek(stream, 0L, SEEK_END);
	size_t end = ftell(stream);
	fseek(stream, pos, SEEK_SET);

	return end;
}

Game read_game_set(void) {
	Game game = {0};
	FILE *file = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(stderr, "Error while opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	size_t total_lines = line_count(file);
	size_t end_of_file = file_size(file);

	char buffer [1024] = {0};
	fgets(buffer, 1023, file);
	total_lines--;
	if( buffer[1023] == '\n' )
		buffer[1023] = '\0';
	else
		buffer[strlen(buffer) - 1] = '\0';

	game.draw_length = 0;
	for(size_t idx=0; idx < strlen(buffer); idx++)
		if( buffer[idx] == ',' )
			game.draw_length++;
	game.draw_length += 2;
	game.draw = calloc(game.draw_length, sizeof(size_t));

	{
		char *ptr = &buffer[0];
		size_t idx = 0;
		while( *ptr != '\n' && *ptr != '\0' && idx < game.draw_length ) {
			char *end = NULL;

			game.draw[idx] = strtoul(ptr, &end, 10);
			idx++;

			if( *end == ',' )
				end++;

			ptr = end;
		}
	}

	fgets(buffer, 1023, file);
	total_lines--;

	while((size_t)ftell(file) != end_of_file) {
		// Reading empty line after draw:
		game.boards_number++;
		game.boards = realloc(game.boards, game.boards_number * 5 * 5 * sizeof(size_t));

		size_t offset = (game.boards_number - 1) * 5 * 5;
		for(size_t idx=0; idx < BOARD_SIZE; idx++)
			fscanf(
				file,
				"%lu %lu %lu %lu %lu\n",
				&game.boards[offset + idx * 5 + 0],
				&game.boards[offset + idx * 5 + 1],
				&game.boards[offset + idx * 5 + 2],
				&game.boards[offset + idx * 5 + 3],
				&game.boards[offset + idx * 5 + 4]
			);

		total_lines -= 5;
	}

	return game;
}

bool board_fill_and_won(size_t *board, Status *status, size_t draw) {
	bool is_in = false;

	for(size_t idx=0; idx < BOARD_SIZE * BOARD_SIZE; idx++)
		if( board[idx] == draw ) {
			status->marked[idx] = is_in = true;
			printf("%lu at position: %lu, %lu: %lu\n", draw, idx / BOARD_SIZE, idx % BOARD_SIZE, board[idx]);
		}

	if( ! is_in )
		return false;

	// Checking lines:
	for(size_t idx=0; idx < BOARD_SIZE; idx++) {
		bool won = status->marked[idx * BOARD_SIZE];
		for(size_t jdx=1; jdx < BOARD_SIZE; jdx++)
			won &= status->marked[idx * BOARD_SIZE + jdx];
		if( won )
			return true;
	}

	// Checking columns:
	for(size_t jdx=0; jdx < BOARD_SIZE; jdx++) {
		bool won = status->marked[jdx];
		for(size_t idx=1; idx < BOARD_SIZE; idx++)
			won &= status->marked[idx * BOARD_SIZE + jdx];
		if( won )
			return true;
	}

	return false;
}

Status play_game(Game game) {
	ChainedLstInt skip = lst_int_new();
	Status *game_status = calloc(game.boards_number, sizeof(struct st_status));
	for(size_t idx=0; idx < game.boards_number; idx++)
		game_status[idx].board_id = idx;

	Status winner = {0};
	for(size_t turn=0; turn < game.draw_length; turn++) {
		size_t draw = game.draw[turn];

		printf("Playing %lu\n", draw);
		bool won = false;
		for(size_t board=0; board < game.boards_number; board++) {
			if( lst_int_in(skip, board) )
				continue;

			game_status[board].last = draw;

			won = board_fill_and_won(
				&game.boards[board * BOARD_SIZE * BOARD_SIZE],
				&game_status[board],
				draw
			);
			if( won ) {
				lst_int_append(skip, board);
				if( game.boards_number == skip->len ) {
					winner = game_status[board];
				}
			}
		}

		if( skip->len == game.boards_number )
			break;
	}

	free(game_status);
	lst_int_free(skip);
	return winner;
}

size_t sum_winning_line_col(size_t *board, bool *marked) {
	size_t sum = 0;

	for(size_t idx=0; idx < BOARD_SIZE * BOARD_SIZE; idx++)
		if( ! marked[idx] )
			sum += board[idx];

	return sum;
}

int main(void) {
	Game game = read_game_set();

	Status winner = play_game(game);
	printf("Winner is board: %lu, with %lu\n", winner.board_id, winner.last);

	printf(
		"win sum: %lu\n",
		sum_winning_line_col(
			&game.boards[winner.board_id * BOARD_SIZE * BOARD_SIZE],
			winner.marked
		)
	);
	printf(
		"Answer is: %lu\n",
		sum_winning_line_col(
			&game.boards[winner.board_id * BOARD_SIZE * BOARD_SIZE],
			winner.marked
		) * winner.last
	);

	free(game.draw), game.draw = NULL;
	free(game.boards), game.boards = NULL;
	return EXIT_SUCCESS;
}
