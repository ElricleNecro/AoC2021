#ifndef INPUT_FILE
#define INPUT_FILE "day7/input"
#endif

#include <math.h>
#include <stdint.h>
#include <limits.h>

#include "utils.c"

void _close_file(FILE **file) {
	fclose(*file);
}

struct array parse_file(void) {
	FILE *file __attribute__((__cleanup__(_close_file))) = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(file, "An error occured when opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct array data = {.size=0, .data=NULL};

	int read_value = 0;
	while( fscanf(file, "%d,", &read_value) == 1 ) {
		data.size += 1;
		data.data = realloc(data.data, sizeof(int) * data.size);
		((int*)data.data)[data.size - 1] = read_value;
	}

	return data;
}

int64_t mean(struct array data) {
	int sum = 0;
	for(size_t idx=0; idx < data.size; idx++)
		sum += ((int*)data.data)[idx];

	return round(sum / (1.0 * data.size));
}

int max(struct array data) {
	int m = 0;
	for(size_t idx=0; idx < data.size; idx++)
		if( m < ((int*)data.data)[idx] )
			m = ((int*)data.data)[idx];

	return m;
}

size_t brute_force(struct array data) {
	size_t best_optimal = LONG_MAX, best = LONG_MAX;
	int maximum = max(data);
	for(int idx=0; idx < maximum; idx++) {
		size_t cost = 0;
		for(size_t jdx=0; jdx < data.size; jdx++) {
			int64_t moves = labs(((int*)data.data)[jdx] - idx);
			cost += moves * (moves + 1) / 2;
		}

		if( cost < best ) {
			best = cost;
			best_optimal = idx;
		}
	}

	printf("Optimal pos: %lu (max: %d)\n", best_optimal, maximum);
	return best;
}

size_t sum(struct array data) {
	size_t res = 0;
	for(size_t idx=0; idx < data.size; idx++)
		res += ((int*)data.data)[idx];

	return res;
}

int main(void) {
	struct array data = parse_file();

	int64_t optimal = sum(data) / (1.0 * data.size);
	size_t cost = 0;
	for(size_t idx=0; idx < data.size; idx++) {
		int64_t moves = labs(((int*)data.data)[idx] - optimal);
		cost += moves * (moves + 1) / 2;
	}

	printf("Answer: %lu\n", cost);

	printf("Brute force: %lu\n", brute_force(data));
	
	return EXIT_SUCCESS;
}
