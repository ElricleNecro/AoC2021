#ifndef INPUT_FILE
#define INPUT_FILE "day7/input"
#endif

#include "utils.c"

void _close_file(FILE **file) {
	fclose(*file);
}

struct array parse_file(void) {
	FILE *file __attribute__((__cleanup__(_close_file))) = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(file, "An error occured when opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct array data = {.size=0, .data=NULL};

	int read_value = 0;
	while( fscanf(file, "%d,", &read_value) == 1 ) {
		data.size += 1;
		data.data = realloc(data.data, sizeof(int) * data.size);
		((int*)data.data)[data.size - 1] = read_value;
	}

	return data;
}

int cmp(const void *a, const void *b) {
	int left = *(int*)a;
	int right = *(int*)b;

	if( left > right )
		return 1;
	else if ( left < right )
		return -1;
	else
		return 0;
}

int main(void) {
	struct array data = parse_file();

	qsort(data.data, data.size, sizeof(int), cmp);

	for(size_t idx=0; idx < data.size; idx++)
		printf("%d\n", ((int*)data.data)[idx]);

	int optimal = ((int*)data.data)[data.size/2];
	size_t cost = 0;
	for(size_t idx=0; idx < data.size; idx++)
		cost += abs(((int*)data.data)[idx] - optimal);

	printf("Answer: %lu\n", cost);
	
	return EXIT_SUCCESS;
}
