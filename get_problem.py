#!/usr/bin/env python3
# coding: utf-8

from configparser import ConfigParser
from logging import getLogger
from pathlib import Path
from typing import Callable, Iterable, TypeVar
from sys import argv, exit

from bs4 import BeautifulSoup
from bs4.element import Tag
from requests import Session

LOGGER = getLogger("aoc")
BASE_DIRECTORY = Path(f"day{day}")
TEMPLATES = Path(".template")
_A = TypeVar('A')


def copy(from_: Path, to: Path, /) -> None:
    try:
        to.write_bytes(from_.read_bytes())
    except Exception as exc:
        return exc


def parse_line(brief: Tag):
    match line.name:
        case "h2":
            yield f"# {line.contents[0]}\n"
        case None:
            yield "\n"
        case "p":
            yield f"{line.contents[0]}\n"
        case "pre":
            yield f"```\n{line.get_text()}```\n"
        case _:
            yield str(line)


def apply(func: Callable[[_A], None], iter: Iterable[_A]) -> None:
    for elem in iter:
        func(elem)


if '__main__' == __name__:
    day = argv[1]

    session = Session()
    config = ConfigParser()
    config.read("cookies.ini")

    brief = BeautifulSoup(
        session.get(
            f"https://adventofcode.com/2021/day/{day}", cookies=config["cookies"]
        ).text,
        features="lxml",
    )
    input_data = session.get(
        f"https://adventofcode.com/2021/day/{day}/input", cookies=config["cookies"]
    )

    with open(BASE_DIRECTORY / "README.md", "w") as f:
        res = brief.find("article")
        process = map(parse_line, res.children)
        apply(f.write, process)

    with open(BASE_DIRECTORY / "input", "w") as f:
        f.write(input_data.content)

    for template_file in TEMPLATES.iterdir():
        if not template_file.is_file():
            continue

        into = BASE_DIRECTORY / template_file.stem
        into.mkdir(parents=True, exist_ok=True)

        if err:= copy(template_file, (into / "part1").with_suffix("." + template_file.stem)) is not None:
            print(err)
            exit(1)

        if err := copy(template_file, (into / "part2").with_suffix("." + template_file.stem)) is not None:
            print(err)
            exit(1)
