# Advent of Code 2021

My personal solutions for the advent of code 2021.

This repository is organised per day. Each day has a:
- `brief.md`: file with the problem summary.
- `test`: file with the example data.
- `input`: my problem input.

Then, there is a subdirectory with the name of the language containing a file for each part of the day.
