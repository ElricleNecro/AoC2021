CC=gcc

OUTDIR=.build
DAY=1
PART_DAY=1
INPUT=input

CFLAGS=-W -Wall -Wextra -g3 -I. -DDEBUG
#-fsanitize=address

.PHONY: run build_dir test clean

$(OUTDIR)/day$(DAY)/c/part$(PART_DAY): day$(DAY)/c/part$(PART_DAY).c stringview.c stringview.h build_dir
	@$(CC) $(CFLAGS) -DINPUT_FILE=\"day$(DAY)/$(INPUT)\" $< stringview.c -o $@ -lm

$(OUTDIR)/test/day$(DAY)/c/part$(PART_DAY): day$(DAY)/c/part$(PART_DAY).c stringview.c stringview.h build_dir
	@$(CC) $(CFLAGS) -DINPUT_FILE=\"day$(DAY)/test\" $< stringview.c -o $@ -lm

run: $(OUTDIR)/day$(DAY)/c/part$(PART_DAY)
	@./$^

test: $(OUTDIR)/test/day$(DAY)/c/part$(PART_DAY)
	@./$^

build_dir:
	@mkdir -p $(OUTDIR)/day$(DAY)/c
	@mkdir -p $(OUTDIR)/test/day$(DAY)/c

clean:
	@rm -rf $(OUTDIR)/day$(DAY)/c
	@rm -rf $(OUTDIR)/test/day$(DAY)/c
