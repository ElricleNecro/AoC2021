#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef INPUT_FILE
#define INPUT_FILE "day1/input"
#endif

struct array {
	/** data */
	int *data;
	/** array size */
	size_t size;
};

size_t line_count(FILE *stream) {
	size_t lines = 0;
	char c = EOF, prev = EOF;

	for(c=fgetc(stream), prev=EOF; c != EOF; prev = c, c = fgetc(stream) )
		if( c == '\n' )
			lines++;

	if( prev != '\n' )
		lines++;

	rewind(stream);

	return lines;
}

struct array read_data(void) {
	FILE *file = NULL;

	if( (file = fopen(INPUT_FILE, "r")) == NULL ) {
		fprintf(stderr, "Error opening file '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	size_t size = line_count(file);
	int *data = malloc(sizeof(int) * size);

	for(size_t idx = 0; idx < size; idx++) {
		int value = -1;
		fscanf(file, "%d\n", &value);
		/*array_set(data, idx, value);*/
		data[idx] = value;
	}

	fclose(file);
	return (struct array){.data = data, .size = size};
}

int main(void) {
	struct array data = read_data();

	size_t count = 0;
	for(size_t idx = 1; idx < data.size-2; idx++) {
		int window1 = data.data[idx-1] + data.data[idx] + data.data[idx+1];
		int window2 = data.data[idx] + data.data[idx+1] + data.data[idx+2];

		if( window2 > window1 ) count++;
	}

	printf("Answer is: %lu.\n", count);
	
	free(data.data);
	return EXIT_SUCCESS;
}
