#include "utils.c"

typedef enum direction_enum {
	FORWARD=0,
	DOWN,
	UP,
} Direction;

struct instruction {
	Direction dir;
	size_t units;
};

ChainedLst parse_instructions(void) {
	FILE *file = NULL;

	if( (file = fopen(INPUT_FILE, "r")) == NULL ) {
		fprintf(stderr, "An error occured while opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	size_t size = line_count(file);
	ChainedLst inst = lst_new();

	for(size_t idx=0; idx < size; idx++) {
		char inst_name[8] = {0};
		size_t units = 0;

		struct instruction *data = NULL;
		if( (data = malloc(sizeof(struct instruction))) == NULL ) {
			fprintf(stderr, "Error while allocating memory: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}

		fscanf(file, "%s %lu\n", inst_name, &units);
		data->units = units;
		if( strcmp(inst_name, "forward") == 0 )
			data->dir = FORWARD;
		else if( strcmp(inst_name, "down") == 0 )
			data->dir = DOWN;
		else if( strcmp(inst_name, "up") == 0 )
			data->dir = UP;
		else {
			fprintf(stderr, "Unknown instruction '%s'.\n", inst_name);
			exit(EXIT_FAILURE);
		}

		lst_append(inst, data);
	}

	fclose(file);
	return inst;
}

int main(void) {
	ChainedLst program = parse_instructions();

	int horizontal = 0;
	int depth = 0;
	int aim = 0;
	for(ChainedLst_elem lst_elem = program->start; lst_elem != NULL; lst_elem = lst_elem->next) {
		struct instruction *elem = lst_elem->data;
		switch(elem->dir) {
			case FORWARD:
				horizontal += elem->units;
				depth += aim * elem->units;
				break;
			case DOWN:
				aim += elem->units;
				break;
			case UP:
				aim -= elem->units;
				break;
		}
	}

	printf("Answer is: %d.\n", horizontal * depth);
	lst_free(program, free);
	return EXIT_SUCCESS;
}
