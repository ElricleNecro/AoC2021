#ifndef INPUT_FILE
#define INPUT_FILE "day6/input"
#endif

#include <stdint.h>

#include "utils.c"

void _close_file(FILE **file) {
	fclose(*file);
}

uint64_t* parse_file(void) {
	FILE *file __attribute__((__cleanup__(_close_file))) = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(file, "An error occured when opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	uint64_t *data = calloc(9, sizeof(uint64_t));

	int read_value = 0;
	while( fscanf(file, "%d,", &read_value) == 1 ) {
		data[read_value] += 1;
	}

	return data;
}

void evolve(uint64_t* data) {
	uint64_t tmp[9] = {0};

	tmp[8] += data[0];
	tmp[6] += data[0];

	for(size_t i=1; i < 9; i++) {
		tmp[i-1] += data[i];
	}

	memcpy(data, tmp, 9 * sizeof(uint64_t));
}

uint64_t count_fish(uint64_t *data) {
	uint64_t count = 0;
	for(size_t i=0; i < 9; i++)
		count += data[i];
	return count;
}

void dump(uint64_t *data) {
	for(size_t i=0; i < 9; i++)
		printf("%lu : %lu\n", i, data[i]);
}

int main(void) {
	uint64_t *data = parse_file();

	for(size_t day=0; day < 256; day++) {
		evolve(data);
	}

	printf("Answer: %lu\n", count_fish(data));

	free(data);
	return EXIT_SUCCESS;
}
