#ifndef INPUT_FILE
#define INPUT_FILE "day6/input"
#endif

#include "utils.c"

void _close_file(FILE **file) {
	fclose(*file);
}

struct array parse_file(void) {
	FILE *file __attribute__((__cleanup__(_close_file))) = fopen(INPUT_FILE, "r");
	if( file == NULL ) {
		fprintf(file, "An error occured when opening '"INPUT_FILE"': %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct array data = {.size=0, .data=NULL};

	int read_value = 0;
	while( fscanf(file, "%d,", &read_value) == 1 ) {
		data.size += 1;
		data.data = realloc(data.data, sizeof(int) * data.size);
		((int*)data.data)[data.size - 1] = read_value;
	}

	return data;
}

int main(void) {
	struct array data = parse_file();

	for(size_t day=1; day <= 80; day++) {
		size_t added_fish = 0;
		for(size_t fish=0; fish < data.size; fish++) {
			int value = ((int*)data.data)[fish];
			value -= 1;

			if( value < 0 ) {
				value = 6;
				added_fish++;
			}

			((int*)data.data)[fish] = value;
		}

		if( added_fish > 0 ) {
			data.size += added_fish;
			data.data = realloc(data.data, data.size * sizeof(int));

			for(size_t len=data.size-added_fish; len < data.size; len++)
				((int*)data.data)[len] = 8;
		}

		/*for(size_t fish=0; fish < data.size; fish++) {*/
			/*printf("%d ", ((int*)data.data)[fish]);*/
		/*}*/
		/*printf("\n");*/
	}

	printf("Answer: %lu\n", data.size);

	array_free(&data);
	return EXIT_SUCCESS;
}
