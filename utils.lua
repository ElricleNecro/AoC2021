local utils = {}

function utils.map(f, ...)
	local args = {...}
	local next, p, s = nil, nil, nil

	if #args == 1 then
		next, p, s = ipairs(args[1])
	else
		next, p, s = args[1], args[2], args[3]
	end

	return function(param, state)
		local value = nil
		state, value = next(param, state)

		if value == nil then
			return nil
		end

		return state, f(value)
	end, p, s
end

function utils.filter(f, ...)
	local args = {...}
	local next, p, s = nil, nil, nil

	if #args == 1 then
		next, p, s = ipairs(args[1])
	else
		next, p, s = args[1], args[2], args[3]
	end

	return function(param, state)
		local value = nil
		state, value = next(param, state)

		while true do
			if value == nil then
				return nil
			end

			if f(value) then
				return state, value
			end

			state, value = next(param, state)
		end
	end, p, s
end

return utils
