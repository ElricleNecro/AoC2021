#ifndef UTILS_C_QRSWJMWI
#define UTILS_C_QRSWJMWI

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#ifndef INPUT_FILE
#define INPUT_FILE "day1/input"
#endif

#ifndef gotoxy
#define gotoxy(x, y) printf("\033[%d;%dH", (y), (x))
#endif

#ifndef getxy
#define getxy(x, y) do { \
	printf("\x1b[6n"); \
	fflush(stdout); \
	scanf("\x1b[%d;%dR", (x), (y)); \
} while(0)
#endif


#ifndef clear_screen
#define clear_screen() printf("\x1b[2J")
#endif
#ifndef cursor_at_top
#define cursor_at_top() printf("\x1b[H")
#endif

#define ARRAY1D(type) typedef struct array1d_t_##type { \
	type *data; \
	size_t size; \
} Array1d_##type; \
Array1d_##type new_array1d_##type(const size_t size) { \
	return (Array1d_##type){.size = size, .data = malloc(size * sizeof(type))}; \
} \
Array1d_##type append_array1d_##type(Array1d_##type data, const type value) { \
	data.data = realloc(data.data, sizeof(type) * (data.size + 1)); \
	data.data[data.size] = value; \
	data.size += 1; \
	return data; \
} \
Array1d_##type remove_element_array1d_##type(Array1d_##type data, size_t idx) { \
	if( idx >= data.size ) \
		return data; \
	if( idx < (data.size - 1) ) \
		memcpy(&data.data[idx], &data.data[idx+1], sizeof(type) * (data.size - idx - 1)); \
	data.size -= 1; \
	return data; \
}

#define ARRAY(type) typedef struct array2d_t_##type { \
	type *data; \
	size_t width, height, size; \
} Array2d_##type

#define LIST(type) typedef struct list2d_t_##type { \
	type ***data; \
	size_t length; \
	size_t *sizes; \
} List2d_##type

#define STACK(type) typedef struct stack_t_##type { \
	type *data; \
	size_t size, capacity; \
} *Stack_##type; \
Stack_##type new_stack_##type(void) { \
	Stack_##type snew = malloc(sizeof(struct stack_t_##type)); \
	snew->data = NULL; \
	snew->size = 0; \
	snew->capacity = 0; \
	return snew; \
} \
Stack_##type new_with_capacity_stack_##type(size_t cap) { \
	Stack_##type snew = malloc(sizeof(struct stack_t_##type)); \
	snew->data = malloc(cap * sizeof(type)); \
	snew->size = 0; \
	snew->capacity = cap; \
	return snew; \
} \
void push_stack_##type(Stack_##type stack, type value) { \
	if( stack->size == stack->capacity ) { \
		stack->capacity += 10; \
		stack->data = realloc(stack->data, stack->capacity * sizeof(type)); \
	} \
	stack->data[stack->size] = value; \
	stack->size++; \
} \
type pop_stack_##type(Stack_##type stack) { \
	stack->size--; \
	return stack->data[stack->size]; \
} \
void free_stack_##type(Stack_##type stack) { \
	if( stack->capacity > 0 ) \
		free(stack->data); \
} \
void free_with_deleter_stack_##type(Stack_##type stack, void (*deleter)(type)) { \
	if( stack->capacity > 0 ) { \
		for(size_t idx=0; idx < stack->capacity; idx++) \
			deleter(stack->data[idx]); \
		free(stack->data); \
	} \
}

/**
 * Count the number of line in a file.
 *
 * @param stream file to count from.
 * @return the number of line present.
 */
size_t line_count(FILE *stream) {
	size_t lines = 0;
	char c = EOF, prev = EOF;

	for(c=fgetc(stream), prev=EOF; c != EOF; prev = c, c = fgetc(stream) )
		if( c == '\n' )
			lines++;

	if( prev != '\n' )
		lines++;

	rewind(stream);

	return lines;
}

/**
 * Determine the length of a line in a file.
 *
 * @param stream file to count from.
 * @return the length of a line.
 */
size_t line_width(FILE *file) {
	char *buf = NULL;
	size_t width = 0;

	getline(&buf, &width, file);
	rewind(file);

	width = strlen(buf);
	if( buf[width-1] == '\n' )
		width--;

	free(buf);
	return width;
}

static struct termios ORIG;

void setup_terminal(void) {
	struct termios term;
	tcgetattr(STDIN_FILENO, &term);
	ORIG = term;

	term.c_lflag &= !(ECHO | ICANON | IEXTEN | ISIG);
	term.c_iflag &= !(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
	term.c_oflag &= !(OPOST);
	term.c_cflag |= CS8;

	term.c_cc[VMIN] = 0;
	term.c_cc[VTIME] = 1;

	tcsetattr(STDIN_FILENO, TCSAFLUSH, &term);
}

void restore_terminal(void) {
	ORIG.c_iflag |= BRKINT | ICRNL | INPCK | ISTRIP | IXON;
	ORIG.c_oflag |= OPOST;
	ORIG.c_lflag |= ECHO | ICANON | IEXTEN | ISIG;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &ORIG);
}

/** Array struct. */
struct array {
	/** data */
	void *data;
	/** array size */
	size_t size;
};

/**
 * Create an array of given size.
 *
 * @param size Array size.
 * @return a struct array.
 */
struct array array_create(size_t size) {
	return (struct array){ .data = (int*)malloc(size), .size = size};
}

/**
 * Free the array.
 *
 * @param arr array to free.
 */
void array_free(struct array *arr) {
	free(arr->data);
	arr->size = 0;
}

bool array_set_int(struct array arr, size_t idx, int value) {
	if( idx >= arr.size ) {
		fprintf(stderr, "ERROR: %lu >= %lu.\n", idx, arr.size);
		exit(EXIT_FAILURE);
	}

	((int*)arr.data)[idx] = value;
	return true;
}

int array_get_int(struct array arr, size_t idx) {
	if( idx >= arr.size ) {
		fprintf(stderr, "ERROR: %lu >= %lu.\n", idx, arr.size);
		exit(EXIT_FAILURE);
	}

	return ((int*)arr.data)[idx];
}

typedef struct _chained_lst_t {
	void *data;

	struct _chained_lst_t *next, *prev;
} *ChainedLst_elem;

typedef struct _chaine_lst_root_t {
	size_t len;

	struct _chained_lst_t *start, *end;
} *ChainedLst;

ChainedLst lst_new(void) {
	ChainedLst root = malloc(sizeof(struct _chaine_lst_root_t));

	root->len = 0;
	root->start = NULL;
	root->end = NULL;

	return root;
}

#define LST_FOREACH(type, var, from, code) { \
	for(ChainedLst_elem _var = from->start; _var != NULL; _var = _var->next) { \
		type var = (type)_var->data; \
		do code while(0); \
	} \
}

ChainedLst lst_append(ChainedLst root, void *data) {
	if( root == NULL ) {
		root = lst_new();
	}

	ChainedLst_elem new = malloc(sizeof(struct _chained_lst_t));

	new->data = data;

	if( root->end != NULL )
		root->end->next = new;

	if( root->start == NULL )
		root->start = new;

	new->prev = root->end;
	new->next = NULL;

	root->end = new;
	root->len++;

	return root;
}

bool lst_in(ChainedLst root, void *data, bool (*cmp)(void*, void*)) {
	if( root == NULL )
		return false;

	for(ChainedLst_elem elem = root->start; elem != NULL; elem = elem->next) {
		if( cmp(data, elem->data) )
			return true;
	}

	return false;
}

void lst_free(ChainedLst root, void (*custom_free)(void*)) {
	if( root == NULL )
		return ;

	ChainedLst_elem elem = root->start;
	while(elem != NULL) {
		ChainedLst_elem cur = elem;
		elem = elem->next;
		if( custom_free != NULL )
			custom_free(cur->data);
		free(cur);
	}

	free(root);
}

#endif /* end of include guard: UTILS_C_QRSWJMWI */
